package com.hoover.hoovernavigator.controller;

import com.hoover.hoovernavigator.model.CleaningResult;
import com.hoover.hoovernavigator.model.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldReturnStatusOKAndCorrectResponseData() {
        Room room = initRoom();
        HttpEntity<Room> request = new HttpEntity<>(room);

        ResponseEntity<CleaningResult> response = testRestTemplate.postForEntity("http://localhost:" + this.port + "/room/cleanup", request, CleaningResult.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());

        CleaningResult cleaningResult = response.getBody();

        assertNotNull(cleaningResult);
        long expectedPatches = 1;
        Integer coordX = 1;
        Integer coordY = 3;
        assertEquals(expectedPatches, cleaningResult.getPatches());
        assertEquals(coordX , cleaningResult.getCoords().get(0));
        assertEquals(coordY , cleaningResult.getCoords().get(1));
    }

    private Room initRoom() {
        Room room = new Room();
        room.setRoomSize(Arrays.asList(5, 5));
        room.setCoords(Arrays.asList(1, 2));
        List<List<Integer>> patches = new LinkedList<>();
        patches.add(Arrays.asList(1, 0));
        patches.add(Arrays.asList(2, 2));
        patches.add(Arrays.asList(2, 3));
        room.setPatches(patches);
        room.setInstructions("NNESEESWNWW");
        return room;
    }
}
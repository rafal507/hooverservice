package com.hoover.hoovernavigator.service;

import com.hoover.hoovernavigator.model.Room;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RoomServiceTest {
    private Room room;
    private RoomService roomService = new RoomService();

    @Before
    public void init() {
        room = new Room();
        room.setRoomSize(Arrays.asList(5, 5));
        room.setCoords(Arrays.asList(1, 2));
        List<List<Integer>> patches = new LinkedList<>();
        patches.add(Arrays.asList(1, 0));
        patches.add(Arrays.asList(2, 2));
        patches.add(Arrays.asList(2, 3));
        room.setPatches(patches);
    }

    @Test
    public void shouldCorrectlyUpdateCurrentHooverPositionWhenHooverIsMovingNorth() {
        //given
        room.setInstructions("N");
        //when
        int expectedY = 3;
        int actualY = roomService.cleanUp(room)
                
                .getCoords()
                .get(1);
        //then
        assertEquals(expectedY, actualY);
    }

    @Test
    public void shouldCorrectlyUpdateCurrentHooverPositionWhenHooverIsMovingSouth() {
        //given
        room.setInstructions("S");
        //when
        int expectedY = 1;
        int actualY = roomService.cleanUp(room).getCoords().get(1);
        //then
        assertEquals(expectedY, actualY);
    }

    @Test
    public void shouldCorrectlyUpdateCurrentHooverPositionWhenHooverIsMovingEast() {
        //given
        room.setInstructions("E");
        //when
        int expectedX = 2;
        int actualX = roomService.cleanUp(room).getCoords().get(0);
        //then
        assertEquals(expectedX, actualX);
    }

    @Test
    public void shouldCorrectlyUpdateCurrentHooverPositionWhenHooverIsMovingWest() {
        //given
        room.setInstructions("W");
        //when
        int expectedX = 0;
        int actualX = roomService.cleanUp(room).getCoords().get(0);
        //then
        assertEquals(expectedX, actualX);
    }

    @Test
    public void shouldCorrectlyUpdateCurrentHooverPositionWhenHooverIsMovingTwiceEastAndNorth() {
        //given
        room.setInstructions("EENN");
        //when
        int expectedX = 3;
        int actualX = roomService.cleanUp(room).getCoords().get(0);
        int expectedY = 4;
        int actualY = roomService.cleanUp(room).getCoords().get(1);
        //then
        assertEquals(expectedX, actualX);
        assertEquals(expectedY, actualY);
    }

    @Test
    public void theHooverShouldStayAtPlaceWhenMovingByIntructionsWillCouseExceedingTheSouthAndWestWall() {
        //given
        room.setCoords(Arrays.asList(0, 0));
        room.setInstructions("SW");
        //when
        int expectedX = 0;
        int actualX = roomService.cleanUp(room).getCoords().get(0);
        int expectedY = 0;
        int actualY = roomService.cleanUp(room).getCoords().get(1);
        //then
        assertEquals(expectedX, actualX);
        assertEquals(expectedY, actualY);
    }

    @Test
    public void theHooverShouldStayAtPlaceWhenMovingByIntructionsWillCouseExceedingTheSouthAndNorthWall() {
        //given
        room.setCoords(Arrays.asList(4, 4));
        room.setInstructions("NE");
        //when
        int expectedX = 4;
        int actualX = roomService.cleanUp(room).getCoords().get(0);
        int expectedY = 4;
        int actualY = roomService.cleanUp(room).getCoords().get(1);
        //then
        assertEquals(expectedX, actualX);
        assertEquals(expectedY, actualY);
    }

    @Test
    public void whenHooverMeetsPatchOfDirtThenNumberOfCleanedPatchesOfDirtIsGrowing() {
        //given
        room.setInstructions("NNESEESWNWW");
        //when
        long expectedPatches = 1;
        long actualPatches = roomService.cleanUp(room).getPatches();
        //then
        assertEquals("Incorrect number of cleaned patches.", expectedPatches, actualPatches);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenDirectionIsNotCorrect() {
        //given
        room.setInstructions("Z");
        //when
        roomService.cleanUp(room);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenDirectionIsEmpty() {
        //given
        room.setInstructions("");
        //when
        roomService.cleanUp(room);
    }
}
package com.hoover.hoovernavigator.model;

import java.util.List;

public class CleaningResult {
    private List<Integer> coords;
    private long patches;

    public List<Integer> getCoords() {
        return coords;
    }

    public void setCoords(List<Integer> coords) {
        this.coords = coords;
    }

    public long getPatches() {
        return patches;
    }

    public void setPatches(long patches) {
        this.patches = patches;
    }
}

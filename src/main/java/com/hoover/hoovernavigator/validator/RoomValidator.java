package com.hoover.hoovernavigator.validator;

import com.hoover.hoovernavigator.model.Room;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

import static java.util.Objects.isNull;

@Component
public class RoomValidator {

    public boolean validateInputData(Room room) {
        return isRoomSizeInvalid
                .or(areCoordinatesInvalid)
                .or(arePatchesInvalid)
                .or(areInstructionsInvalid)
                .test(room);
    }

    private static Predicate<Room> isRoomSizeInvalid = room -> isNull(room.getRoomSize())
            || room.getRoomSize().size() != 2;

    private static Predicate<Room> areCoordinatesInvalid = room -> isNull(room.getCoords())
            || room.getCoords().size() != 2;

    private static Predicate<Room> arePatchesInvalid = room -> isNull(room.getPatches())
                    || room.getPatches()
                    .stream()
                    .anyMatch(v -> v.size() != 2);

    private static Predicate<Room> areInstructionsInvalid = room -> isNull(room.getInstructions());
}

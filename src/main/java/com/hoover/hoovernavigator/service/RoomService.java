package com.hoover.hoovernavigator.service;

import com.hoover.hoovernavigator.model.CleaningResult;
import com.hoover.hoovernavigator.model.Room;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class RoomService {

    public CleaningResult cleanUp(Room room) {
        String[] directions = room.getInstructions().toUpperCase().split("");
        AtomicInteger xCoord = new AtomicInteger(room.getCoords().get(0));
        AtomicInteger yCoord = new AtomicInteger(room.getCoords().get(1));
        AtomicLong removedPatches = new AtomicLong();

        Arrays.stream(directions).forEach(direction -> {
            moveByDirection(room, direction, xCoord, yCoord);
            cleanUpIfHooverIsOnPatchOfDirt(room, removedPatches, xCoord, yCoord);
        });

        CleaningResult cleaningResult = new CleaningResult();
        cleaningResult.setCoords(Arrays.asList(xCoord.get(), yCoord.get()));
        cleaningResult.setPatches(removedPatches.get());
        return cleaningResult;
    }

    private void moveByDirection(Room room, String direction, AtomicInteger xCoord, AtomicInteger yCoord) {
        List<Integer> roomSize = room.getRoomSize();
        switch (direction) {
            case "E":
                if (xCoord.get() < roomSize.get(0) - 1) {
                    xCoord.incrementAndGet();
                }
                break;
            case "W":
                if (xCoord.get() > 0) {
                    xCoord.decrementAndGet();
                }
                break;
            case "N":
                if (yCoord.get() < roomSize.get(1) - 1) {
                    yCoord.incrementAndGet();
                }
                break;
            case "S":
                if (yCoord.get() > 0) {
                    yCoord.decrementAndGet();
                }
                break;
            default:
                throw new IllegalArgumentException("Incorrect direction instructions.");
        }
    }

    private void cleanUpIfHooverIsOnPatchOfDirt(Room room, AtomicLong removedPatches, AtomicInteger xCoord, AtomicInteger yCoord) {
        room.getPatches()
                .parallelStream()
                .filter(v -> v.get(0).equals(xCoord.get()) && v.get(1).equals(yCoord.get()))
                .findAny()
                .ifPresent(p -> {
                    room.getPatches().remove(p);
                    removedPatches.incrementAndGet();
                });
    }
}

package com.hoover.hoovernavigator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HooverNavigatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(HooverNavigatorApplication.class, args);
    }
}

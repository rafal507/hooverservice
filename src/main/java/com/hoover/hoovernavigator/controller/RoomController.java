package com.hoover.hoovernavigator.controller;

import com.hoover.hoovernavigator.model.CleaningResult;
import com.hoover.hoovernavigator.model.Room;
import com.hoover.hoovernavigator.service.RoomService;
import com.hoover.hoovernavigator.validator.RoomValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomValidator roomValidator;

    @PostMapping("/cleanup")
    public ResponseEntity<CleaningResult> cleanUp(@RequestBody Room room) {

        if (isNull(room) || roomValidator.validateInputData(room)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        CleaningResult cleaningResult = roomService.cleanUp(room);
        return new ResponseEntity<>(cleaningResult, HttpStatus.OK);
    }
}
